wrk-dir := $(shell pwd)
src-dir := $(wrk-dir)/extension
dst-dir := $(wrk-dir)/build
pem-dir := $(HOME)/Sync/Apps/crx-pem

ext-name := $(shell basename $(wrk-dir))

$(dst-dir)/$(ext-name).crx: $(src-dir)/manifest.json
	@mkdir -p $(dst-dir)
	@echo ID: $(shell openssl rsa -pubout -outform DER -in $(pem-dir)/$(ext-name).pem 2>/dev/null | sha256sum | head -c32 | tr 0-9a-f a-p)
	@npm install
	@npx crx3 -p $(pem-dir)/$(ext-name).pem -o $@ $(src-dir)

